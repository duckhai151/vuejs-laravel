<?php

use Illuminate\Http\Request;
//JWTAuth
Route::group(['prefix' => 'auth'], function () {
    Route::post('register', 'AuthController@register')->middleware('admin');
    Route::post('login', 'AuthController@login');
    Route::post('getuser', 'AuthController@getuser')->middleware('admin');
    Route::get('getpage', 'UserController@getPage');
    Route::post('logout', 'AuthController@logout')->middleware('jwt.auth');
    Route::get('user', 'AuthController@user')->middleware('jwt.auth');
    Route::get('permissions', 'UserController@getPermissions');
});
// Route::middleware('jwt.refresh')->get('/token/refresh', 'AuthController@refresh');
Route::get('setfavorite/{id}', 'UserController@favorite');
Route::get('unfavorite/{id}', 'UserController@unfavorite');
Route::get('getfavorite', 'UserController@getFavorite')->middleware('admin');
Route::resource('image', 'ImageController');
