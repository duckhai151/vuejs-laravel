import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)

axios.defaults.baseURL = 'http://127.0.0.1:8080/api/';
axios.defaults.headers.common['Authorization'] = localStorage.getItem('token');


export default class APIService {
    constructor(){
        this.arr = 'arr API';
    }

    static async getData(url) {
        let data = await axios.get(url);
        console.log('apiService', data);
        return data.data;
    }

    static getData1(url) {
        axios.get(url + '?token=' + localStorage.getItem('token'))
        .then(response => {
            return response.data;
        });
    }

    static postData(url, data, config = null)
    {   
        axios.post(url + '?token=' + localStorage.getItem('token'), data).then(function (response) {
            console.log(response.data.success)
        })
    }
}

