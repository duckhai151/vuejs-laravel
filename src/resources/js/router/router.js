import Vue from 'vue'
import VueRouter from 'vue-router'
import App from '../App.vue';
import Home from '../components/Home.vue';
import CreatePost from '../components/post/CreatePost.vue';
import IndexPost from '../components/post/IndexPost.vue';
import EditPost from '../components/post/EditPost.vue';
import Login from '../components/user/Login.vue';
import IndexUser from '../components/user/IndexUser.vue';
import Register from '../components/user/Register.vue';
import UploadImage from '../components/image/UploadImage.vue';
import IndexImage from '../components/image/IndexImage.vue';
import Dashboard from '../components/admin/Dashboard.vue';
import Test1 from '../components/all/Test1.vue';
import ListImage from '../components/all/ListImage.vue';
import ListFavorite from '../components/all/ListFavorite.vue';

Vue.use(VueRouter)
const check = false;
let routes = [
    {
        name: 'master',
        path: '/',
        component: App,
        children: [
            {
                name:'home',
                path:'/home',
                component: Home,
            },
            {
                name:'favorite',
                path:'/favorite',
                component: ListFavorite,
            },
            {
                name: 'index-image',
                path: '/image',
                component: IndexImage
            },
        ]
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'dashboard',
        path: '/dashboard',
        component: Dashboard,
        children: [
            {
                name: 'user-index',
                path: '/user',
                component: IndexUser
            },
            {
                name: 'register',
                path: '/register',
                component: Register
            },
            {
                name: 'test1',
                path: '/img/upload',
                component: Test1
            },
            {
                name: 'list-img',
                path: '/img/list',
                component: ListImage
            },
        ]
    },
  ];

  const router = new VueRouter({
      routes,
      mode: 'history',
  });
  export default router;