import Vue from 'vue'
import Vuex from 'vuex'
import VueAxios from 'vue-axios'
import axios from 'axios'
import { app } from '../app'
Vue.use(Vuex)
Vue.use(VueAxios, axios)

axios.defaults.baseURL = 'http://127.0.0.1:8080/api/';

export const store = new Vuex.Store({
    state: {
        permissions: []
    },
    actions: {
        loadPermissions ({ commit }) {
            axios.get('auth/permissions?token=' + localStorage.getItem('token'))
            .then(r => r.data.permissions)
            .then(permissions => {
                commit('SET_PERMISSIONS', permissions)
            })
        },
        setLang({commit}, payload) {
            commit('SET_LANG', payload)
        },
    },
    mutations: {
        SET_PERMISSIONS (state, permissions) {
            state.permissions = permissions
        },
        SET_LANG (state, payload) {
            app.$i18n.locale = payload
        }
       
    }
})