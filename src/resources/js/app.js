require('./bootstrap');

window.Vue = require('vue')

import Vue from 'vue'
import { store } from './store/store'
import router from './router/router'
import VueI18n from 'vue-i18n'
import vnMessage from './lang/vi.json'
import enMessage from './lang/en.json'

Vue.use(VueI18n)
const messages = {
    vi: vnMessage,
    en: enMessage,
}
const i18n = new VueI18n({
  locale: 'en', // set locale
  messages,
  fallbackLocale: 'en',
});
  
export const app = new Vue({
  el: '#app',
  i18n,
  store: store,
  router,
});
