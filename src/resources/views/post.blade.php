<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
        <link href="css/sb-admin-2.css" rel="stylesheet">
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <meta name="csrf-token" value="{{ csrf_token() }}" />
    </head>
    <body>
      <div id="app">
        <router-view></router-view>
      </div>
      <!-- Bootstrap core JavaScript-->
      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

      <!-- Core plugin JavaScript-->
      <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

      <!-- Custom scripts for all pages-->
      <script src="js/sb-admin-2.min.js"></script>

      <!-- Page level plugins -->
      {{-- <script src="vendor/chart.js/Chart.min.js"></script> --}}

      <!-- Page level custom scripts -->
      {{-- <script src="js/demo/chart-area-demo.js"></script> --}}
      {{-- <script src="js/demo/chart-pie-demo.js"></script>  --}}
      <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>

    </body>
</html>
