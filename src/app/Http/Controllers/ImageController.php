<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Carbon\Carbon; 

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all()
        ->toArray();
        return [
            'images' => $images,
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    private function changeImageName($file)
    {
        $imageName = Carbon::now().'.'.$file->getClientOriginalExtension();
        return $imageName;
    }

    private function moveImage($file, $fileName)
    {
        $file->move('images', $fileName);
    }

    private function saveImage($arrayImage)
    {
        $image = new Image();
        $image->insert($arrayImage);
    }

    public function store(Request $request)
    {
        $uploadedFiles = $request->pics;
        $arrayImage = array();
        foreach ($uploadedFiles as $file){
            $imageName = $this->changeImageName($file);
            $this->moveImage($file, $imageName);
            array_push($arrayImage, ['name' => 'abcd', 'image' => $imageName]);
        }
        $this->saveImage($arrayImage);
        return response()->json(['success'=>'You have successfully upload image.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \Log::info($id);
        $image = Image::find($id);
        $image->delete();
        return response()->json('Delete success');
    }
}
