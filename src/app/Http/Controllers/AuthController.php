<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use App\User;
use App\Http\Resources\PostCollection;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!($token = JWTAuth::attempt($credentials))) {
            return response()->json([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'Invalid Credentials.'
            ], Response::HTTP_BAD_REQUEST);
        }
        return response()->json(['token' => $token, 'status' => 'success'], Response::HTTP_OK);
    }

    public function register(Request $request)
    {
        \Log::info($request);   
        $user = new User();
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = $request->password;
        $user->role_id = 0; 
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user,
        ], Response::HTTP_OK);
    }

    public function user(Request $request)
    {
        $user = Auth::user();
        if ($user) {
            return response($user, Response::HTTP_OK);
        }

        return response(null, Response::HTTP_BAD_REQUEST);
    }

    public function logout(Request $request) {    
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(array('message' => 'You have successfully logged out.', 'status' => 'success'), Response::HTTP_OK);
        } catch (JWTException $e) {
            return response()->json('Failed to logout, please try again.', Response::HTTP_BAD_REQUEST);
        }
    }


    public function refresh()
    {
        return response(JWTAuth::getToken(), Response::HTTP_OK);
    }

    public function getuser(Request $request) 
    {
      $perPage = $request->get('perPage');
      $page = $request->get('page');
      $users = User::limit($perPage)
                ->offset(($page-1)*$perPage)
                ->get()
                ->toArray();
    
        return [
            'users' => $users,
        ];
    }   
    
    public function getPage()
    {
        return response()->json(array("totalpage" => User::count()));
    }   

    public function check()
    {
        
    }

    public function getPermission(Request $request)
    {
        JWTAuth::parseToken();
        $user = JWTAuth::authenticate();
        $permissions = $user->role->permissions;
    }
}
