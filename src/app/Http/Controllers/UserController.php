<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PostCollection;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Image;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Favorite;
use App\Role;
use App\Permission_Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    public function login(Request $request)
    {
        if(Auth::check()) {
            return response()->json(array("success" => 1));
        } else {
            $login = [
                'email' => $request->email,
                'password' => $request->password
            ];
            if (Auth::attempt($login)) {
                return response()->json(array("success" => 1, "name" => Auth::user()->email));              
            } 
            else {
                return response()->json(array("success" => 0));
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        if(Auth::check()) {
            return response()->json(array("success" => 1));   
        } else {
            return response()->json(array("success" => 0));   
        }
    }

    public function getUser()
    {
        return Auth::user();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pagination(Request $request) 
    {
      $perPage = $request->get('perPage');
      $page = $request->get('page');
      return new PostCollection(User::limit($perPage)->offset(($page-1)*$perPage)->get());
    }   
    
    public function getPage()
    {
        return response()->json(array("totalpage" => User::count()));
    }

    public function favorite(Request $request, $id)
    {
        JWTAuth::setToken($request->get('token'));
        $user = JWTAuth::authenticate();
        $user->favorites()->attach($id);
        return response()->json($user);
    }

    public function unfavorite(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user->favorites()->detach($id);
        return response()->json('Unfavorited');
    }

    public function getFavorite(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $images = $user->favorites->toArray();
        $arrImageId = Favorite::select('image_id')->where('user_id','=', $user->id)->get()->toArray();
        return [
            'images' => $images,
            'arrImageId' => $arrImageId,
        ];
    }

    public function getPermissions(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $permissions = $user->role->permissions->toArray();
        return [
            'permissions' => $permissions,
        ];
    }

    
}
