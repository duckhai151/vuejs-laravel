<?php

namespace App\Http\Middleware;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Closure;
use App\User;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $allow = [
        
    ];

    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $permissions = $user->role->permissions;
        } catch (JWTException $e) {
            if ($e instanceof TokenInvalidException){
                return response()->json([
                'status_code' => 401,
                'message' => 'Token không hợp lệ.',
            ]);
            }else if ($e instanceof TokenExpiredException){
                return response()->json([
                'status_code' => 401,
                'message' => 'Token đã hết hạn'
                ]);
            }else{
                return response()->json([
                'status_code' => 401,
                'message' => 'Token chứng thực không được tìm thấy.'
                ]);
            }
        }
        \Log::info($request->path());
        foreach($permissions as $permission) {
            if($permission->url == $request->path()) {
                return $next($request);
        } 
        }
        return response()->json([
            'message' => 'Khong co quyen'
        ]);

    }  
}
