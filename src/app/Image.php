<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public function user()
    {
        return $this->belongsToMany('App\User', 'favorites', 'image_id', 'user_id');
    }
}
