<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	// ['name'=>'role_id','email'=>'role_id@gmail.com','password'=>Hash::make('123456')],
            ['name'=>'test1','email'=>'test1@gmail.com','password'=>Hash::make('123456'),'role_id' => 1],
            ['name'=>'test2','email'=>'test2@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test3','email'=>'test3@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test4','email'=>'test4@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test5','email'=>'test5@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test6','email'=>'test6@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test7','email'=>'test7@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test8','email'=>'test8@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test9','email'=>'test9@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test10','email'=>'test10@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test11','email'=>'test11@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test12','email'=>'test12@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test13','email'=>'test13@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
            ['name'=>'test14','email'=>'test14@gmail.com','password'=>Hash::make('123456'),'role_id' => 0],
        ]);    

    }
}
